# Frontend Mentor - Huddle landing page with single introductory section solution

This is a solution to the [Huddle landing page with single introductory section challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/huddle-landing-page-with-a-single-introductory-section-B_2Wvxgi0). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

## Overview

### The challenge

Users should be able to:

- View the optimal layout for the page depending on their device's screen size
- See hover states for all interactive elements on the page

### Screenshot

![Desktop](./screenshots/desktop.jpg)
![Mobile](./screenshots/mobile.jpg)

### Links

- Solution URL: [Gitlab Repository](https://gitlab.com/frontend-mentor-challenge/huddle-landing-page)
- Live Site URL: [Live](https://huddle-landing-page-2fn3.onrender.com)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow
- [Normalize](https://necolas.github.io/normalize.css/) - CSS Reset
- [Font Awesome](https://fontawesome.com/) - Social Media Icons
- [Google Fonts](https://fonts.google.com/)

### What I learned

I  learned how to use pseudo classes for active states.
I also learned how to use grid and flexbox

### Continued development

### Useful resources

- [Figma](https://figma.com) - For design

## Author

- Website - [Issac Leyva](In construction)
- Frontend Mentor - [@issleyva](https://www.frontendmentor.io/profile/issleyva)

## Acknowledgments